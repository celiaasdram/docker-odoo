#!/bin/bash

# Variables
resourceGroupName="OCC_ASD_DOCKERCELIA"
vnetName="Vnet_OCC_ASD_DOCKERCELIA"
location="francecentral"
subnetName="Subnet_Vnet_OCC_ASD_CDOCKERCELIA"
vmName="DOCKERCELIA"
vmSize="Standard_B2s"
adminUser="celia"
adminPassword="Azerty123456"
dockerNetwork="DOCKERNET"
vnet_address_prefix="10.0.6.0/24"

# Création du groupe de ressources
if ! az group show --name "$resourceGroupName" &>/dev/null; then
    az group create --name "$resourceGroupName" --location "$location"
fi

# Création du réseau virtuel
az network vnet create \
    --resource-group $resourceGroupName \
    --name $vnetName \
    --location $location \
    --address-prefix $vnet_address_prefix

# Création du sous-réseau "admin"
az network vnet subnet create \
    --resource-group $resourceGroupName \
    --vnet-name $vnetName \
    --name admin \
    --address-prefix 10.0.6.0/28

# Création des sous-réseaux supplémentaires
for (( i=0; i<=255; i+=16 ))
do
    subnet_address="10.0.6.$i/28"
   subnet_name="Subnet_Vnet_OCC_ASD_CDOCKERCELIA$(($i / 16))"

    az network vnet subnet create \
        --resource-group $resourceGroupName \
        --vnet-name $vnetName \
        --name $subnet_name \
        --address-prefix $subnet_address
done

# Création de la VM
az vm create \
    --resource-group "$resourceGroupName" \
    --name "$vmName" \
    --image 'Debian11' \
    --size "$vmSize" \
    --admin-username "$adminUser" \
    --admin-password "$adminPassword" \
    --vnet-name "$vnetName" \
    --subnet "admin"

# Attente que la machine virtuelle soit prête
sleep 60

# Exécution des commandes sur la VM
az vm run-command invoke -g "$resourceGroupName" -n "$vmName" --command-id RunShellScript --scripts '
#!/bin/bash

#Installation de Docker

sudo apt update && sudo apt upgrade -y
sudo apt install -y docker-compose
sudo apt install -y docker.io

#Les commandes du script sarrêtent ici faute de droits Azure, jai vu quon pouvait corriger ca avec un az -login mais nayant pas       testé par manque de temps

sudo usermod -aG docker $adminUSER

#Redémarrage de la VM pour appliquer lajout de luser au groupe Docker
sudo reboot

sleep 5

# Création du réseau Docker
sudo docker network create '"$dockerNetwork"'

# Démarrage des conteneurs avec Docker Compose
cat <<EOF > docker-compose.yml
version: '3.1'
services:
  db:
    image: postgres
    container_name: db
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_USER=odoo
      - POSTGRES_PASSWORD=odoo
    volumes:
      - odoo-db-data:/var/lib/postgresql/data

  web:
    image: odoo:16.0
    container_name: odoo
    depends_on:
      - db
    ports:
      - "80:8069"
    volumes:
      - odoo-web-data:/var/lib/odoo
      - odoo-config:/etc/odoo
      - odoo-addons:/mnt/extra-addons
    environment:
      - PGHOST=db
      - PGUSER=odoo
      - PGPASSWORD=odoo
      - PGDATABASE=odoo
    restart: always

volumes:
  odoo-db-data:
  odoo-web-data:
  odoo-config:
  odoo-addons:  
EOF

sudo docker-compose up -d
'