# Utiliser l'image officielle d'Odoo
FROM odoo:latest

# Définir l'utilisateur comme root pour effectuer des opérations d'installation
USER root

# Installer les dépendances PostgreSQL client
RUN apt-get update && apt-get install -y postgresql-client

# Créer un répertoire pour stocker les données PostgreSQL
RUN mkdir -p /var/lib/postgresql/data

# Définir le propriétaire de ce répertoire sur l'utilisateur "odoo"
RUN chown -R odoo /var/lib/postgresql

# Définir le répertoire de travail comme /var/lib/postgresql/data
WORKDIR /var/lib/postgresql/data

# Exposer le port 8069 pour Odoo
EXPOSE 8069

# Exposer le port 5432 pour PostgreSQL
EXPOSE 5432

# Exécuter le serveur Odoo au démarrage
CMD ["odoo"]