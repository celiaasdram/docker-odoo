Création d'une VM contenant un serveur web Odoo ainsi qu'une base de données Postgres persistante.

Nous allons déployer une VM Debian12 en SSH sur laquelle nous allons configurer deux conteneurs, un serveur web Odoo ainsi qu'une base de données persistante Postgres que l'on peut déployer à l'aide d'un Dockerfile. Nos deux conteneurs seront basés sur le réseau Docker "DOCKERNET" pour qu'elles puissent communiquer entre elles.

Le repo contient plusieures étapes du TP (dockerfile, scripts individuels de déploiement en Bash, ainsi que le docker-compose) et le script de déploiement entier 'SCRIPT.sh'

Le script de déploiement est écrit en Bash et contient le déploiement de la création de ressources Azure: groupe de ressources, réseau (vnet&subnet ainsi que groupe de sécurité non configuré), provisionnée avec l'installation des packets docker ainsi que dockercompose, l'ajout de l'utilisateur admin au groupe docker, création du réseau docker et création d'un docker-compose.yml contenant l'installation et la configuration du serveur web Odoo & de la base de données Postgres.

Le script s'arrête au milieu du shell executable dans la VM par manque de droit azure, la ligne sur laquelle le script s'arrête est commentée.

Coût de la VM pour 1 mois 24h/24h (Standard_B2s): 51,236 €/mois