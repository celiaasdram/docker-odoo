#!/bin/bash

# ressurce group name
resource_group="OCC_ASD_Célia"

# vnet name
vnet_name="Vnet_OCC_ASD_Célia"

# loca
location="francecentral"

# network range and cidr
vnet_address_prefix="10.0.6.0/24"

# create vnet
az network vnet create \
    --resource-group $resource_group \
    --name $vnet_name \
    --location $location \
    --address-prefix $vnet_address_prefix

# create admin subnet
az network vnet subnet create \
    --resource-group $resource_group \
    --vnet-name $vnet_name \
    --name admin \
    --address-prefix 10.0.6.0/28

# create subnet
for (( i=0; i<=255; i+=16 ))
do
    subnet_address="10.0.6.$i/28"
    subnet_name="Subnet_Vnet_OCC_ASD_Célia$i-i+=1"

    az network vnet subnet create \
        --resource-group $resource_group \
        --vnet-name $vnet_name \
        --name $subnet_name \
        --address-prefix $subnet_address
done
