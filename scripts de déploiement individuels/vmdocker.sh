#!/bin/bash

# var
resource_group="OCC_ASD_Célia"
location="francecentral"
vm_name="docker-celia"
vm_size="Standard_B2s"
admin_user="celia"
admin_password="Azerty123456"
virtual_network="Vnet_OCC_ASD_Célia"
subnet_name="Subnet_Vnet_OCC_ASD_Célia1"

# create vm debian11
az vm create \
  --resource-group $resource_group \
  --name $vm_name \
  --image 'Debian11' \
  --size $vm_size \
  --admin-username $admin_user \
  --admin-password $admin_password \
  --vnet-name $virtual_network \
  --subnet $subnet_name

# shell docker
az vm run-command invoke -g $resource_group -n $vm_name --command-id RunShellScript --scripts '
#!/bin/bash

sudo apt update
sudo apt install -y docker.io
sudo systemctl start docker
sudo systemctl enable docker
'