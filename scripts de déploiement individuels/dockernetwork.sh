#!/bin/bash

#create docker network
docker network create mon_reseau

#connect containers to network
docker network connect mon_reseau container1
docker network connect mon_reseau container2

#start them
docker start container1
docker start container2

#update and install ping command
bash -c "apt-get update && apt-get install -y iputils-ping"

